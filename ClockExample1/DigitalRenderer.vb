﻿Imports ClockExample1

Public Class DigitalRenderer
    Implements IClockRenderer

    Private _font as font

    public sub New()
        _font = New Font(FontFamily.GenericMonospace,24,FontStyle.Regular, GraphicsUnit.Point)
    End sub

    Public Sub Render(target As Graphics, time As Date) Implements IClockRenderer.Render
        target.Clear(Color.Black)

        target.DrawString(time.TimeOfDay.ToString(), _font, Brushes.Green,new PointF(20,20))
    End Sub
End Class
