﻿Public Class Form1

    Public Property Renderer As IClockRenderer
    Private Sub tmrClick_Tick(sender As Object, e As EventArgs) Handles tmrClick.Tick
        pnlClock.Invalidate()
    End Sub

    Private Sub pnlClock_Paint(sender As Object, e As PaintEventArgs) Handles pnlClock.Paint
        Renderer.Render(e.Graphics, DateTime.Now)
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Renderer = New AnalogRenderer()
        
        lstRenderer.Items.Add(New AnalogRenderer())
        lstRenderer.Items.Add(New DigitalRenderer())
    End Sub

    Private Sub lstRenderer_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstRenderer.SelectedIndexChanged
        Renderer = CType(lstRenderer.SelectedItem, IClockRenderer)
        pnlClock.Invalidate()
    End Sub
End Class
