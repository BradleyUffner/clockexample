﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lstRenderer = New System.Windows.Forms.ListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tmrClick = New System.Windows.Forms.Timer(Me.components)
        Me.pnlClock = New System.Windows.Forms.Panel()
        Me.SuspendLayout
        '
        'lstRenderer
        '
        Me.lstRenderer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.lstRenderer.FormattingEnabled = true
        Me.lstRenderer.IntegralHeight = false
        Me.lstRenderer.Location = New System.Drawing.Point(15, 25)
        Me.lstRenderer.Name = "lstRenderer"
        Me.lstRenderer.Size = New System.Drawing.Size(176, 282)
        Me.lstRenderer.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Renderer:"
        '
        'tmrClick
        '
        Me.tmrClick.Enabled = true
        Me.tmrClick.Interval = 1000
        '
        'pnlClock
        '
        Me.pnlClock.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.pnlClock.Location = New System.Drawing.Point(197, 25)
        Me.pnlClock.Name = "pnlClock"
        Me.pnlClock.Size = New System.Drawing.Size(304, 282)
        Me.pnlClock.TabIndex = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(513, 319)
        Me.Controls.Add(Me.pnlClock)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lstRenderer)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents lstRenderer As ListBox
    Friend WithEvents Label1 As Label
    Friend WithEvents tmrClick As Timer
    Friend WithEvents pnlClock As Panel
End Class
