﻿Imports ClockExample1

Public Class AnalogRenderer
    Implements IClockRenderer

    Public Sub Render(target As Graphics, time As Date) Implements IClockRenderer.Render
        target.Clear(Color.Black)
        target.FillEllipse(Brushes.White,new Rectangle(20,20,200,200))

        'Do the rest of the stuff to draw the clock
        
    End Sub
End Class
